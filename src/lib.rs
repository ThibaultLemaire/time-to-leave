/*
    Time to Leave
    Copyright (C) 2021  Thibault Lemaire

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::{convert::TryInto, fmt::Display};

use chrono::prelude::*;
use iced_web::{
    button, executor::Null, text_input, Align, Application, Button, Clipboard, Column, Command,
    Element, HorizontalAlignment, Length, Row, Space, Text,
};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
pub fn main() -> () {
    // Initialize debugging for when/if something goes wrong.
    console_error_panic_hook::set_once();

    TimeToLeave::run(());
}

#[derive(Default, Serialize, Deserialize)]
struct WorkDay {
    carry_over: i64,
    due: u16, // If your employer expects you to work more than 18h a day, you should probably quit
    worklog: WorkLog,
}

impl WorkDay {
    fn balance(&self) -> i64 {
        self.carry_over
            + self
                .worklog
                .worked()
                .iter()
                .map(|wp| wp.duration().num_seconds())
                .sum::<i64>()
            - self.due as i64
    }
}

struct TimeToLeave {
    record: WorkDay,
    settings: Settings,
    persisted: PersistedState,

    window: web_sys::Window,
    local_storage: web_sys::Storage,
    /// Mitigate user forgetting to stop the counter by periodically logging the current time.
    /// This assumes the user is working as long as they have the tab open (and it's in the recording state).
    /// So if they forget to stop it but close their tab/browser/computer, we can deduce approximately when they left (the watchdog is only active as long as the tab is open)
    last_seen_active_cb: Closure<dyn Fn()>,
    watchdog: Option<IntervalHandle>,
    stopwatch_button: button::State,
    due_time_input: (text_input::State, String),
    worklog_inputs: Vec<(text_input::State, text_input::State)>,
    worklog_edit_buffer: Option<(usize, String)>,
}

impl Application for TimeToLeave {
    type Message = Message;
    type Executor = Null;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Message>) {
        let window = web_sys::window().expect("no window");
        let local_storage = window
            .local_storage()
            .expect("error getting localstorage")
            .expect("no local storage");
        let settings: Settings = local_storage.load("settings").unwrap_or_default();
        let (record, persisted) = match local_storage.load::<PersistedState>("state") {
            Some(persisted) => (
                local_storage
                    .load::<WorkDay>(&persisted.day)
                    .unwrap_or_default(),
                persisted,
            ),
            None => match local_storage.load::<v2::PersistedState>(v2::SavePath::new("state")) {
                Some(v2::PersistedState { day, balance }) => {
                    if day == Local::now().date_naive() {
                        panic!("Migrating from v2 to v3 in the same day is unsupported.")
                    }
                    (
                        WorkDay {
                            carry_over: balance,
                            ..Default::default()
                        },
                        Default::default(),
                    )
                }
                None => Default::default(),
            },
        };
        // Periodically log the time when actively recording
        let last_seen_active_cb = {
            let local_storage = local_storage.clone();
            Closure::<dyn Fn()>::new(
                move || match local_storage.load::<PersistedState>("state") {
                    Some(PersistedState { day }) if day == Local::now().date_naive() => {
                        local_storage.save("last-seen-active", &Utc::now())
                    }
                    _ => {}
                },
            )
        };
        let mut new = Self {
            record,
            due_time_input: (<_>::default(), settings.hours_a_day.to_hm_string()),
            settings,
            persisted,
            window,
            local_storage,
            last_seen_active_cb,
            stopwatch_button: <_>::default(),
            worklog_inputs: <_>::default(),
            worklog_edit_buffer: None,
            watchdog: None,
        };
        new.start_or_stop();
        (new, Command::none())
    }

    fn title(&self) -> String {
        String::from("Time To Leave.")
    }

    fn update(&mut self, message: Message, _clipboard: &mut Clipboard) -> Command<Message> {
        match message {
            Message::StopWatchButtonPressed => self.start_or_stop(),
            Message::DueTimeInputChanged(new) => self.due_time_input.1 = new,
            Message::DueTimeSubmitted => {
                if let Ok(parsed) = NaiveTime::parse_from_str(&self.due_time_input.1, "%Hh%Mm") {
                    self.settings.hours_a_day = (
                        parsed
                            .hour()
                            .try_into()
                            .expect("Parsing should never output more than 23 hours"),
                        parsed
                            .minute()
                            .try_into()
                            .expect("Parsing should never output more than 59 minutes"),
                    );
                    self.record.due = self.settings.hours_a_day.to_seconds();
                    self.local_storage.save("settings", &self.settings);
                }
                self.due_time_input.1 = self.settings.hours_a_day.to_hm_string();
            }
            Message::WorklogInputChanged(index, new_value) => {
                self.worklog_edit_buffer = Some((index, new_value))
            }
            Message::WorklogTimeSubmitted => {
                if let Some((index, buffer)) = self.worklog_edit_buffer.as_mut() {
                    if let Ok(parsed) = NaiveTime::parse_from_str(buffer, "%H:%M") {
                        let (worklog_index, sub_index) = (*index / 2, *index % 2);
                        let target_slot = &mut self.record.worklog.worked_mut()[worklog_index];
                        let mut new_value = target_slot.clone();
                        let today = self.persisted.day;
                        *(if sub_index == 0 {
                            &mut new_value.start
                        } else {
                            &mut new_value.end
                        }) = today
                            .and_time(parsed)
                            .and_local_timezone(Local)
                            .single()
                            .expect("Back-and-forth local time conversion failed")
                            .with_timezone(&Utc);

                        if new_value.start < new_value.end {
                            *target_slot = new_value;
                            self.local_storage.save(&today, &self.record);
                        }
                    }
                }
                self.worklog_edit_buffer = None;
            }
        }
        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        let stopwatch_button = Button::new(
            &mut self.stopwatch_button,
            Text::new(match self.record.worklog {
                WorkLog::Closed { .. } => "▶️",
                WorkLog::Open { .. } => "⏸️",
            }),
        )
        .on_press(Message::StopWatchButtonPressed);
        let start_time = match self.record.worklog {
            WorkLog::Closed { .. } => None,
            WorkLog::Open { started, .. } => Some(started.with_timezone(&Local)),
        };
        let balance = self.record.balance();
        let estimated_leave_time = Text::new({
            if balance < 0 {
                (start_time.unwrap_or_else(Local::now)
                    + chrono::Duration::seconds(-balance + self.settings.lunch_break_offset()))
                .format("%H:%M")
                .to_string()
            } else {
                "You may leave now.".into()
            }
        })
        .size(40);

        let balance_text = Text::new({
            let total_seconds = if balance < 0 {
                // Show yesterday's balance (carry-over)
                self.record.carry_over
            } else {
                // Workshift complete, show overtime
                balance
            };
            let total_minutes = total_seconds / 60;
            format!("{:+}h{}m", total_minutes / 60, total_minutes % 60)
        })
        .size(30);

        let due_time_config = text_input::TextInput::new(
            &mut self.due_time_input.0,
            "",
            &self.due_time_input.1,
            Message::DueTimeInputChanged,
        )
        .on_submit(Message::DueTimeSubmitted)
        .size(30);

        let mut central_column = Column::new()
            .padding(20)
            .align_items(Align::Center)
            .push(stopwatch_button)
            .push(info_item("Estimated leave time:", estimated_leave_time))
            .push(info_item("Balance:", balance_text))
            .push(info_item("Today's shift:", due_time_config));
        if let Some(time) = start_time {
            // Widths are ugly empirical pixel approximations of the worklog items below
            central_column = central_column.push(
                Row::new()
                    .align_items(Align::Center)
                    .spacing(10)
                    .push(Space::with_width(Length::Fill))
                    .push(
                        Text::new(time.format("%H:%M").to_string())
                            .width(Length::Shrink)
                            .horizontal_alignment(HorizontalAlignment::Left)
                            .size(20),
                    )
                    .push(
                        Text::new("-")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .width(Length::Units(35))
                            .size(20),
                    )
                    .push(
                        Text::new("...")
                            .horizontal_alignment(HorizontalAlignment::Center)
                            .width(Length::Units(110))
                            .size(20),
                    )
                    .push(Space::with_width(Length::Fill)),
            );
        }
        let worklog = self.record.worklog.worked();
        self.worklog_inputs.resize(worklog.len(), <_>::default());
        let edit_buffer = &self.worklog_edit_buffer;
        central_column = worklog
            .iter()
            .enumerate()
            .rev()
            .zip(self.worklog_inputs.iter_mut())
            .fold(
                central_column,
                |column, ((index, wp), (start_input, end_input))| {
                    let start_index = index * 2;
                    let end_index = start_index + 1;
                    let total_minutes = wp.duration().num_minutes();
                    column.push(
                        Row::new()
                            .align_items(Align::Center)
                            .spacing(10)
                            .push(Space::with_width(Length::Fill))
                            .push(displayed_worklog_time(
                                edit_buffer,
                                start_input,
                                &wp.start,
                                start_index,
                            ))
                            .push(
                                Text::new("-")
                                    .horizontal_alignment(HorizontalAlignment::Center)
                                    .width(Length::Shrink)
                                    .size(20),
                            )
                            .push(displayed_worklog_time(
                                edit_buffer,
                                end_input,
                                &wp.end,
                                end_index,
                            ))
                            .push(
                                Text::new(format!(
                                    "({}h{}m)",
                                    total_minutes / 60,
                                    total_minutes % 60,
                                ))
                                .horizontal_alignment(HorizontalAlignment::Left)
                                .width(Length::Fill)
                                .size(20),
                            ),
                    )
                },
            );

        central_column.into()
    }
}

fn displayed_worklog_time<'a>(
    edit_buffer: &Option<(usize, String)>,
    state: &'a mut text_input::State,
    datetime: &DateTime<Utc>,
    index: usize,
) -> text_input::TextInput<'a, Message> {
    let formatted_datetime: String;
    text_input::TextInput::new(
        state,
        "",
        match edit_buffer {
            Some((edit_index, buffer)) if *edit_index == index => buffer,
            _ => {
                formatted_datetime = datetime.with_timezone(&Local).format("%H:%M").to_string();
                &formatted_datetime
            }
        },
        move |new_value| Message::WorklogInputChanged(index, new_value),
    )
    .on_submit(Message::WorklogTimeSubmitted)
    // Will fail to approximate the appropriate width with any other font than the one I use for development :D
    .width(Length::Units(55))
    .size(20)
}

fn info_item<'a>(label: &str, value: impl Into<Element<'a, Message>> + Widget) -> Row<'a, Message> {
    Row::new()
        .spacing(10)
        .align_items(Align::Center)
        .push(
            Text::new(label)
                .horizontal_alignment(HorizontalAlignment::Right)
                .width(Length::Fill)
                .size(25),
        )
        .push(value.width(Length::Fill))
}

impl TimeToLeave {
    fn start_or_stop(&mut self) {
        let utc_now = Utc::now();
        let today = Local::now().date_naive();
        // Set up the periodic logging of time
        let mut start_watch_dog = {
            let watchdog = &mut self.watchdog;
            let window = &self.window;
            let func = &self.last_seen_active_cb;
            move || {
                *watchdog = window.set_interval(chrono::Duration::minutes(5), func).ok();
            }
        };

        if today == self.persisted.day {
            self.record.worklog = match std::mem::take(&mut self.record.worklog) {
                WorkLog::Closed { worked } => {
                    start_watch_dog();
                    WorkLog::Open {
                        started: utc_now,
                        worked,
                    }
                }
                WorkLog::Open {
                    started,
                    mut worked,
                } => {
                    self.watchdog = None;
                    self.local_storage.erase("last-seen-active");
                    worked.push(WorkedPeriod {
                        start: started,
                        end: utc_now,
                    });
                    WorkLog::Closed { worked }
                }
            };
        } else {
            // New day
            let previous = self.persisted.day;
            let mut corrected = false;
            self.record.worklog = match std::mem::take(&mut self.record.worklog) {
                WorkLog::Open {
                    started,
                    mut worked,
                } => {
                    match self.local_storage.load::<DateTime<Utc>>("last-seen-active") {
                        Some(last_seen_active)
                            if DateTime::<Local>::from(last_seen_active).date_naive()
                                == previous =>
                        {
                            corrected = true;
                            worked.push(WorkedPeriod {
                                start: started,
                                end: last_seen_active,
                            })
                        }
                        _ => {}
                    }
                    WorkLog::Closed { worked }
                }
                closed => closed,
            };
            if corrected {
                self.local_storage.save(&previous, &self.record);
            }
            self.persisted.day = today;
            self.record = WorkDay {
                carry_over: self.record.balance(),
                due: self.settings.hours_a_day.to_seconds(),
                worklog: WorkLog::Open {
                    started: utc_now,
                    worked: vec![],
                },
            };
            start_watch_dog();
        }

        self.local_storage.save(&today, &self.record);
        self.local_storage.save("state", &self.persisted);
    }
}

pub struct IntervalHandle {
    interval_id: i32,
}

impl Drop for IntervalHandle {
    fn drop(&mut self) {
        if let Some(window) = web_sys::window() {
            window.clear_interval_with_handle(self.interval_id);
        }
    }
}

trait WindowExtensions {
    fn set_interval(
        &self,
        timeout: chrono::Duration,
        func: &Closure<dyn Fn()>,
    ) -> Result<IntervalHandle, JsValue>;
}

impl WindowExtensions for web_sys::Window {
    /// SAFETY: The ref'ed closure MUST outlive the returned handle
    // TODO: Use lifetimes to enforce this?
    fn set_interval(
        &self,
        timeout: chrono::Duration,
        func: &Closure<dyn Fn()>,
    ) -> Result<IntervalHandle, JsValue> {
        Ok(IntervalHandle {
            interval_id: self.set_interval_with_callback_and_timeout_and_arguments_0(
                func.as_ref().unchecked_ref(),
                timeout.num_milliseconds() as i32,
            )?,
        })
    }
}

#[derive(Serialize, Deserialize)]
enum WorkLog {
    Open {
        started: DateTime<Utc>,
        worked: Vec<WorkedPeriod>,
    },
    Closed {
        worked: Vec<WorkedPeriod>,
    },
}

impl WorkLog {
    fn worked(&self) -> &Vec<WorkedPeriod> {
        match self {
            Self::Open { worked, .. } => worked,
            Self::Closed { worked } => worked,
        }
    }
    fn worked_mut(&mut self) -> &mut Vec<WorkedPeriod> {
        match self {
            Self::Open { worked, .. } => worked,
            Self::Closed { worked } => worked,
        }
    }
}

impl Default for WorkLog {
    fn default() -> Self {
        Self::Closed {
            worked: Default::default(),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
struct WorkedPeriod {
    start: DateTime<Utc>,
    end: DateTime<Utc>,
}

impl WorkedPeriod {
    fn duration(&self) -> chrono::Duration {
        self.end.signed_duration_since(self.start)
    }
}

#[derive(Serialize, Deserialize)]
struct Settings {
    hours_a_day: (u8, u8),
    lunch_time: (u8, u8),
    lunch_duration: (u8, u8),
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            hours_a_day: (7, 0),
            lunch_time: (12, 30),
            lunch_duration: (1, 30),
        }
    }
}

impl Settings {
    fn lunch_break_offset(&self) -> i64 {
        let now = Local::now().naive_local();
        let (hour, min) = self.lunch_time;
        let lunch_time = now
            .date()
            .and_hms_opt(hour as _, min as _, 0)
            .expect("invalid lunch break time");
        if now < lunch_time {
            let (hours, minutes) = self.lunch_duration;
            (hours as i64 * 60 + minutes as i64) * 60
        } else {
            0
        }
    }
}

#[derive(Serialize, Deserialize)]
struct PersistedState {
    day: NaiveDate,
}

impl Default for PersistedState {
    fn default() -> Self {
        Self {
            day: Local::now()
            .date_naive()
            // Forcing start_or_stop() to trigger a day roll-over
            .pred_opt()
            .expect("Congratulations, Time Lord. You managed to trick my app into thinking we're at the dawn of (computer) time! Please open an issue to tell me how you did that")
        }
    }
}

#[derive(Debug, Clone)]
enum Message {
    StopWatchButtonPressed,
    DueTimeInputChanged(String),
    DueTimeSubmitted,
    WorklogInputChanged(usize, String),
    WorklogTimeSubmitted,
}

trait StoreState {
    fn load<T: DeserializeOwned>(&self, key: impl ToSavePath) -> Option<T>;
    fn save<T: ?Sized + Serialize>(&self, key: impl ToSavePath, value: &T);
    fn erase(&self, key: impl ToSavePath);
}

trait ToSavePath {
    fn to_save_path(self) -> String;
}

trait AsHoursMinutes {
    fn to_hm_string(&self) -> String;
    fn to_seconds(&self) -> u16;
}

trait Widget {
    fn width(self, width: Length) -> Self;
}

impl Widget for iced_web::Text {
    fn width(self, width: Length) -> Self {
        self.width(width)
    }
}

impl<'a, Message> Widget for iced_web::TextInput<'_, Message> {
    fn width(self, width: Length) -> Self {
        self.width(width)
    }
}

impl AsHoursMinutes for (u8, u8) {
    fn to_hm_string(&self) -> String {
        let (hours, minutes) = *self;
        format!("{}h{}m", hours, minutes)
    }

    fn to_seconds(&self) -> u16 {
        let (hours, minutes) = *self;
        (hours as u16 * 60 + minutes as u16) * 60
    }
}

impl<T: Display> ToSavePath for T {
    fn to_save_path(self) -> String {
        format!(
            "{}/v{}/{}",
            built_info::PKG_NAME,
            built_info::PKG_VERSION_MAJOR,
            self
        )
    }
}

impl StoreState for web_sys::Storage {
    fn load<T: DeserializeOwned>(&self, key: impl ToSavePath) -> Option<T> {
        self.get_item(&key.to_save_path())
            .expect("error reading from localstorage")
            .map(|serialized| {
                ron::de::from_str(&serialized).expect("failed to read non empty-slot")
            })
    }

    fn save<T: ?Sized + Serialize>(&self, key: impl ToSavePath, value: &T) {
        self.set_item(
            &key.to_save_path(),
            &ron::ser::to_string(&value).expect("serialization error"),
        )
        .expect("error writing to localstorage")
    }

    fn erase(&self, key: impl ToSavePath) {
        self.remove_item(&key.to_save_path())
            .expect("error removing from localstorage");
    }
}

pub mod built_info {
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

mod v2 {
    use crate::*;

    pub struct SavePath {
        path: String,
    }

    impl SavePath {
        pub fn new(key: impl Display) -> Self {
            Self {
                path: format!("{}/v2/{}", built_info::PKG_NAME, key),
            }
        }
    }

    impl ToSavePath for SavePath {
        fn to_save_path(self) -> String {
            self.path
        }
    }

    #[derive(Serialize, Deserialize)]
    pub struct PersistedState {
        pub balance: i64,
        pub day: NaiveDate,
    }
}
