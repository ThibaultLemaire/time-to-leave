{
  description = "Browser-based workday time tracker";

  inputs = {
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { self
    , nixpkgs
    , rust-overlay
    , ...
    }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; overlays = [ (import rust-overlay) ]; };
      rust-wasm32 = pkgs.rust-bin.stable.latest.default.override {
        targets = [ "wasm32-unknown-unknown" ];
      };
      rust-wasm32-minimal = pkgs.rust-bin.stable.latest.minimal.override {
        targets = [ "wasm32-unknown-unknown" ];
      };
      common = with pkgs;  [
        wasm-pack
        wasm-bindgen-cli # Must be matched by wasm-bindgen's version in Cargo.toml.
      ];
    in
    {
      devShells.x86_64-linux.default =
        with pkgs;
        mkShell {
          buildInputs = common ++ [
            rust-wasm32
            just
            nixpkgs-fmt
          ];
        };

      defaultPackage.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.hello;

      ci-docker-image = with pkgs; dockerTools.buildImage {
        name = "registry.gitlab.com/thibaultlemaire/time-to-leave";
        tag = "latest";

        copyToRoot = common ++ [
          rust-wasm32-minimal
          busybox
          stdenv.cc
          cacert # So cargo can https
        ];

        config.Cmd = [ "/bin/sh" ];
      };

    };
}
