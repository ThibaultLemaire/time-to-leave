# Time To Leave

A very simple time tracker for your workday.

## I have a question but I can't (easily) find the answer anywhere

If you have a question about the project (technical or not), feel free to [open an issue], or just [email me].

[open an issue]: https://gitlab.com/ThibaultLemaire/time-to-leave/-/issues
[email me]: thibault.lemaire@protonmail.com

## Useful commands

This project uses [just] to collect useful commands. To get a list of available commands:

```
just --list
```

[just]: https://github.com/casey/just
