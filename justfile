build:
    wasm-pack build --target web --out-dir static/ --mode no-install

# Serve static files locally (You should build before)
serve:
    #! /usr/bin/env nix-shell
    #! nix-shell -p python3 -i sh
    python -m http.server --directory static/

# Format nix files
format-nix:
    nixpkgs-fmt .

# Build docker image for CI/CD. Note that *you need docker installed*. I would add docker to the nix shell, but you need the docker daemon installed on your system anyway
build-docker:
    docker load < $(nix build --print-out-paths --no-link .#ci-docker-image)

# Starts the CI docker image with the source mounted at /src and enters a shell within.
ci-shell: build-docker
    docker run --rm --interactive --tty --volume="$(pwd)":/src registry.gitlab.com/thibaultlemaire/time-to-leave:latest

# Push CI/CD docker image to Gitlab container registry
push-docker user pass: build-docker
    docker login registry.gitlab.com -u {{user}} -p {{pass}}
    docker push registry.gitlab.com/thibaultlemaire/time-to-leave

# You might want to run this if you have never used rustup, or if you have never compiled WASM projects
rustup-setup:
    rustup override set stable
    rustup target add wasm32-unknown-unknown


# IDK what happened but one day cargo bailed on me and I had to do that 🤷
rustup-reinstall:
    rustup toolchain uninstall stable
    just rustup-setup
